﻿Function localadmin {
#"Attempt" can be left at 1, change "maxAttempt" to your pref
$attempt = 1
$maxAttempt = 5

do {
    #Prompt for username
    $targetUser = Read-Host "Enter user ID - ex. smithj01 or js1234  "
    
    #make sure it's a good user
    try {
        Get-ADUser $targetUser -ErrorAction Stop | Out-Null
        $validUser = $true
    }
    #loop back and try again if it isn't
    catch {
        $attempt++
    }
} until (($validUser) -or ($attempt -gt $maxAttempt))

if (-not($validUser)) {
    Write-Host "`nUser account not found`nExiting..."
    pause
    exit
}
else {
    Write-Host "`nUser account found, proceeding..."
}

#Kurt - you want to reset the attempt counter here?

do {
    #prompt for hostname
    $targetHost = Read-Host "Enter computer name "

    #make sure it's online
    try { 
        Test-Connection -ComputerName $targetHost -Count 1 -Quiet | Out-Null
        $validHost = $true
    }
    #loop back and try again if it isn't
    catch {
        $attempt++
    }
} until (($validHost) -or ($attempt -gt $maxAttempt))

if (-not($validHost)) {
    Write-Host "`nTarget computer not found`nExiting..."
    pause
    exit
}
else {
    Write-Host "`nTarget computer found, proceeding..."
}

$addRemoveFlag = Read-Host "'1' to Add, '2' to Remove user from local Admin group."

$domainName = "corp.pvt"
$adminGroup = [ADSI]"WinNT://$targetHost/Administrators,group"
$User = [ADSI]"WinNT://$DomainName/$targetUser,user"

try {
    if ($addRemoveFlag -eq 1) {
        Write-Host "Adding $targetuser from local Admin group on $targethost"
        $adminGroup.Add($User.Path)
    }
    elseif ($addRemoveFlag -eq 2) {
        Write-Host "Removing user"
        $adminGroup.Remove($User.Path)
    }
    else {
        Write-Host "You did it wrong."
    }
    
    Write-Host -ForegroundColor Green "Successfully completed command for `'$targetUser`' on `'$targetHost`'"
} catch {
    Write-Host "Didn't work."
}	
}