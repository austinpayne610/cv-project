#Teams Audit Script
#Developed by Austin Payne 07/2020
#This is developed to audit Teams in an organization and decide if they need to be archived. 

Function Teams_Audit {
    $Report = [System.Collections.Generic.List[Object]]::new()
    Write-Host "Getting Team list"
    $Teams = Get-Team
    Write-Host "Processing Team list information"
    foreach ($Team in $Teams){
        $T = [PSCustomObject][Ordered]@{
            Displayname = $Team.Displayname
            GroupID = $Team.groupid
            Owners = $Owner.count = Get-TeamUser -groupid $Team.groupid -Role Owner
            Member = $Member.count = Get-teamuser -groupid $Team.groupid -Role Member
        }
        $Report.add($T)
    }
    $Report | Export-CSV -NoTypeInformation -path C:/temp
}

