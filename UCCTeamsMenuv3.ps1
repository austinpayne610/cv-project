#UCCTeamsMenuv3 function. 
#Designed to be multiple Functions. 
#Created by Austin Payne June, 2020. 

<#
Designed to be used as a Module, once installed can be called with UCCMenuv3 from Powershell window. 

Will requeste O365 sign in information then once connected will present option in laymans to modify, create, delete and other options for sharepoint and teams pages and channels. 

#>

Function UCCMenuv3{
    Write-Host "Please wait while the function confirms you have the correct Modules installed and imported.
    "
    Start-Sleep -seconds 2
    ModCheck
    Start-Sleep -seconds 2
    Connect
    Creds
    Do {
        Menu
        $selection = Read-Host "Please type the number that corresponds with your query selection"
        Choice 
    } 
    Until ($selection -eq 0){Exit}
}
Function ModCheck{
    #Check for and install Module needed. 
    if (Get-Module -ListAvailable -Name MicrosoftTeams) {
        Import-Module -Name MicrosoftTeams
        } 
    Else {
        Write-Host "Please close the script and Install the MicrosoftTeams Module. 

        Register-PSRepository -Name PSGalleryInt -SourceLocation https://www.poshtestgallery.com/ -InstallationPolicy Trusted
        Install-Module -Name MicrosoftTeams -RequiredVersion 1.0.22 -SkipPublisherCheck"
        }
    #This is to be able to restore a Team, option 15.
    if (Get-module -ListAvailable -Name Azurerms){
        Import-Module AZ
        }
    Else {
        Write-Host "Please close the script and install the Module AZ.
        
        Install-Module AZ"
        }
}
Function Connect{
    $Admin = Read-Host "Enter your AD creds. (vxxxxxx)"
    Connect-microsoftteams -Accountid $Admin@thryv.com
}
Function Creds{
    Write-Host "Follow the prompts to enter the varible values. 
    If no value to enter, simple hit enter and keep the varible empty. "
    $Global:TeamName = Read-Host "Please enter the Displayname of the Target Team"
    $Global:ChannelName = Read-Host "Please enter the Displayname of the Target Channel"
    $Global:UserName = Read-Host "Please enter the AD Userid of the Target user (vxxxxxx)"
}
Function Verify{
    Write-Host $TeamName
    Write-Host $ChannelName
    Write-Host $UserName
    $Verify1 = Read-Host "Is this still the target Variable(y or n)"
    If ($Verify1 -eq "n") {Creds}
}
Function Menu{
    Write-Host "
    ================ UCC Teams Query ================"
    Write-Host ""
    Write-Host "1: User's assigned Teams and their Channels."
    Write-Host "2: Search for a Team/Channel."
    Write-Host "3: Create Team."
    Write-Host "4: Create Channel."
    Write-Host "5: Remove Team."
    Write-Host "6: Remove Channel."
    Write-Host "7: Add User to Team."
    Write-Host "8: Add User to Private Channel."
    Write-Host "9: Remove User from Team."
    Write-Host "10: Remove User from Private Channel."
    Write-Host "11: Change Description on a Team."
    Write-Host "12: Change Description on a Channel."
    Write-Host "13: Get details for a Team."
    Write-Host "14: Get Private Channel current members"
    Write-Host "15: Restore a deleted Team. Windows PC Only"
    Write-Host "0: Press '0' to quit."
    #Archive Teams does not have a command at this time.
    #Write-Host "XX: Create Teams Room service account."

    Write-Host ""
    Write-Host ""
}
Function Choice{
    switch ($selection){ #Below are the numbers matching to each selection in the Menu. 
        '1' {
            Verify
            Try{$TGroupID = Get-Team -User $UserName@domain.com}
            Catch{}

            foreach ($Team in $TGroupID) {Get-TeamChannel -GroupId $Team.groupid |Format-Table $Team.Displayname, Displayname, MembershipType}
            Pause
            } 
        '2' {
            $WC = Read-Host "Enter the Wildcard Variable"
            $E = Read-Host "Are you searching DisplayName or Description"
            if ($E -eq "DisplayName") {
                foreach ($Team in Get-Team){
                    if ($Team.DisplayName -like "*$WC*"){
                        Write-Host ""
                        $Team.DisplayName 
                        Get-TeamChannel -GroupId $Team.groupid |Format-Table DisplayName, Description, MembershipType}
                        Get-TeamUser -groupid $Team.groupid |Format-Table User, MembershipType
                    } 
            }
            elseif ($E -eq "Description"){
                foreach ($Team in Get-Team){
                    if ($Team.Description -like "*$WC*"){
                        $Team.DisplayName 
                        $Team.Description
                        Get-TeamChannel -GroupId $Team.groupid |Format-Table DisplayName, Description, MembershipType}
                    } 
            }
            Else {
                Write-Host "Incorrect input."
                Return 
            }
            Pause
            }
        '3' {
            $Owner = Read-Host "Provide the New Teams Owner AD UserID"
            $Display = Read-Host "
            Provide the desired DisplayName(dept - subdept/project)"
            $Vis = Read-Host "
            Provide Team Visibility, Public or Private"     
            $Description = Read-Host "
            Enter the Team Description"

            $Team = New-Team -DisplayName $Display -Owner $Owner@thryv.com -Visibility $Vis -Description $Description 

            $settings = Read-Host "What level of Guest access is allowed(Full;Restricted)"

            if ($settings.lowercase -eq "restricted") {
                Set-Team -Groupid $Team.groupid -AllowGuestCreateUpdateChannels 0 -AllowGuestDeleteChannels 0
            }

            $option = Read-Host "How many members would you like to add"
            #Plan is to add up to 10 users as an option. 
            if ($option -ge 10) {
                Write-host "Too many users, select option 7 to bulk add."
            }
            for ($i = 0; $i -lt $option; $i++) {
                $user = read-host "Enter username(vxxxxxx)"
                Add-TeamUser -groupid $Team.groupid -user $User
            }
            #Up to 5 Channels
            $Channel = Read-Host "Would you like to add channels(Max 5)"
            if ($option -ge 5) {
                Write-host "Too many Channels. Follow the next prompts to bulk add."
                Start-sleep -seconds 2
                Write-Host "Create a .csv file with the following catagories to bulk create channels."
                Get-Teamchannel -groupid $Team.groupid |Format-Table Displayname, membershiptype
                $Path = Read-Host "Enter the file Path including the File name.csv"

                $csv = import-csv $Path
 
                foreach ($line in $csv) {
                    New-Teamchannel -Groupid $Team.groupid -DisplayName $_.DisplayName -Membershiptype $_.Membershiptype
                }
            }
            for ($i = 0; $i -lt $Channel; $i++) {
                $Channel = read-host "Enter Desired DisplayName"
                New-teamchannel -groupid $Team.groupid -Displayname $Channel
            }
            Get-Team -GroupId $Team.GroupId 
            Get-TeamUser -GroupId $Team.GroupId 
            Get-TeamChannel -GroupId $Team.GroupId
            Pause
            } 
        '4' {
            Verify
            $Display = Read-Host "
            Please provide the Channel Display Name 
            (50 characters or less, cant contain # % & * { } / \ : < > ? + |)"
            $Description = Read-Host "
            Please enter the Channel Description (Up to 1024 Characters)"
            $Vis = Read-Host "
            (Case Sensitive) Standard or Private"
            $Group = Get-Team -DisplayName $TeamName
            $Channel = New-TeamChannel -GroupId $Group.Groupid -DisplayName $Display -Description $Description -MembershipType $Vis 

            If ($vis -eq "Private"){
                    Get-TeamUser -GroupId $Group.groupid |Format-Table user, role

                    Write-Host "Here are the list of members in the Team and their Roles."

                    $option = Read-Host "How many members would you like to add"
                    #Plan is to add up to 10 users as an option. 
                    if ($option -ge 10) {
                        Write-host "Too many Users. Follow the next prompts to bulk add."
                        Start-sleep -seconds 2
                        Write-Host "Create a .csv file with the following catagories to bulk Add Users."
                        Get-TeamchannelUser -groupid $Team.groupid |Format-Table User
                        $Path = Read-Host "Enter the file Path including the File name.csv"

                        $csv = import-csv $Path
 
                        foreach ($line in $csvdemo) {
                            New-TeamchannelUser -Groupid $Team.groupid -user $_.User -Role $_.Role
                        }
                    }
                    for ($i = 0; $i -lt $option; $i++) {
                        $user = read-host "Enter username(vxxxxxx)"
                        Add-TeamChannelUser -groupid $Team.groupid -DisplayName $Channel -user $User
                    }
                }
            Get-TeamChannel -Groupid $Group.Groupid -DisplayName $Display
            Pause
            }
        '5' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName 
            $Check = Read-Host "Please confirm you want to remove user "$UserName"from Team "$TeamName"(y or n)"
            If ($Check -eq "n"){Return}
            Remove-Team -GroupId $Teamg.groupid

            Get-TeamUser -GroupId $Teamg.groupid
            Pause
            }
        '6' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName
            $Check = Read-Host "Please confirm you want to remove user "$UserName"from Team "$TeamName"(y or n)"
            If ($Check -eq "n"){Return}
            Remove-TeamChannel -GroupId $Teamg.groupid -DisplayName $ChannelName

            Get-TeamChannel -GroupId $Teamg.groupid
            Pause
            }  
        '7' {
            Verify
            $Role = Read-Host "
            Will user be a Member or Owner? "
            $Teamg = Get-Team -DisplayName $TeamName

            Add-TeamUser -GroupId $Teamg.groupid -User $UserName@thryv.com
            if ($Role -eq "Private"){
                Add-TeamUser -GroupId $Teamg.groupid -User $UserName@thryv.com -Role $Role
            }
            Get-TeamUser -GroupId $Teamg.groupid
            Pause
            }
        '8' {
            Verify 
            $Response = Write-Host "Is the user above already a member of the Team?(yes or no)"
            If ($Response -eq "yes"){
            }
            elseif ($Response -eq "no") {
                Write-Host "Please add user as a member or owner of the Team then return to this option."
                return
            }
            $Role = Read-Host "
            Will user be a Member or Owner of the Channel?"
            $Teamg = Get-Team -DisplayName $TeamName
            #To add a new member they need to be a member first.
            Add-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName -User $UserName@thryv.com
            do {While ($Role -eq 'Owner')
                    {Add-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName -User $UserName@thryv.com -Role $Role

                    Write-Host ""
                    Write-Host "User added as Owner."
                    return
                    }
                } While ( $Role -eq 'Member' ){
                    Write-Host ""
                    Write-Host "User added as Member."
                    return
                    }
            Get-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName
            Pause
            }
        '9' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName
            $Check = Read-Host "Please confirm you want to remove user "$UserName"from Team "$TeamName"(y or n)"
            If ($Check -eq "n"){Return}
            Remove-TeamUser -GroupId $Teamg.groupid -User $UserName@thryv.com

            Get-TeamUser -GroupId $Teamg.groupid
            Pause
            }
        '10' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName
            $Check = Read-Host "Confirm you wish to remove "$UserName" from Channel "$ChannelName"(y or n)"
            If ($Check -eq "n"){return}
            Remove-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName -User $UserName@thryv.com

            Get-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName
            Pause
            }
        '11' {
            Verify
            Write-Host "Here is the current Description."`n 
            Get-Team -DisplayName $TeamName |Format-Table Description 
            $Description = Read-Host "
            Please provide the Descriprion"
            $Teamg = Get-Team -DisplayName $TeamName
            Set-Team -GroupId $Team.groupid -Description $Description

            Get-Team -GroupId $Team.groupid |Format-Table Displayname, Description
            Pause
            }
        '12' {
            Verify
            Write-Host "Here is the current Description."`n 
            $Teamg = Get-Team -DisplayName $TeamName 
            Get-TeamChannel -Groupid $Teamg.groupid -DisplayName $ChannelName |Format-Table Description 
            $Description = Read-Host "Please provide the Description"
            Set-TeamChannel -GroupId $Teamg.groupid -CurrentDisplayName $ChannelName -Description $Description
            
            Get-TeamChannel -GroupId $Team.groupid |Format-Table Displayname, Description
            Pause
            }
        '13' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName 
            Write-Host ""
            
            Get-Team -DisplayName $TeamName |Format-Table GroupID, Description, Visibility
            Write-Host ""
            Get-TeamUser -GroupId $Teamg.groupid |Format-Table User, Name, Role
            Write-Host ""
            Get-TeamChannel -GroupId $Teamg.groupid |Format-Table DisplayName, MembershipType
            Write-Host ""
            Pause
            }
        '14' {
            Verify
            $Teamg = Get-Team -DisplayName $TeamName

            Get-TeamChannelUser -GroupId $Teamg.groupid -DisplayName $ChannelName |Format-Table User, Name, Role
            Pause
            }
        '15'{
            Connect-AZuread -AccountId $Admin@thryv.com 
            Get-AzureADMSDeletedGroup

            $S = Read-Host "Please copy and paste the GroupID of the Team you wish to restore."

            Restore-azureadmsdeletedDirectoryObject -id $S

            Write-Host "Please allow up to 24 hours for the Team to return to the Application. 
            All previous members, Owners, Channel, Chats, and content will be restored. "
            Get-Team -Groupid $S 
            Pause
            }
        #'XX' { 
            #Create Teams Room Service Account
            #https://docs.microsoft.com/en-us/microsoftteams/rooms/with-office-365
            #
            #}
#        {#Set specific Group back to $True or $False ; https://techcommunity.microsoft.com/t5/microsoft-teams/allow-or-block-guest-users-from-a-specific-team-in-microsoft/m-p/175918#
#        $GroupID = get-unifiedgroup -Identity <Insert SMTP or Identity> | Select-Object -ExpandProperty ExternalDirectoryObjectId
#        $SettingID = Get-AzureADObjectSetting -TargetType Groups -TargetObjectID $GroupID | select-object -expandproperty ID
#        remove-azureadobjectsetting -id $settingid -targettype Groups -TargetObjectID $GroupID
#        $template = Get-AzureADDirectorySettingTemplate | ? {$_.displayname -eq "group.unified.guest"}
#        $settingsCopy = $template.CreateDirectorySetting()
#        $settingsCopy["AllowToAddGuests"]=$False
#        New-AzureADObjectSetting -TargetType Groups -TargetObjectId $groupID -DirectorySetting $settingsCopy}
#        }
#       {Set all Groups/Teams to 'AllowToAddGuests' == $False ; https://techcommunity.microsoft.com/t5/microsoft-teams/allow-or-block-guest-users-from-a-specific-team-in-microsoft/m-p/175918#
#       $groupID = Get-UnifiedGroup -ResultSize Unlimited | Select-Object -ExpandProperty ExternalDirectoryObjectId
#       Foreach ($Groups in $GroupID) {
#       $template = Get-AzureADDirectorySettingTemplate | ? {$_.displayname -eq "group.unified.guest"}
#       $settingsCopy = $template.CreateDirectorySetting()
#       $settingsCopy["AllowToAddGuests"]=$False
#       New-AzureADObjectSetting -TargetType Groups -TargetObjectId $groups -DirectorySetting $settingsCopy
#       } 
#       }
    }
}